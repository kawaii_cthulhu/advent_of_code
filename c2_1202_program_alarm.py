from pathlib import Path


TASK_ID = 2


def get_inputs(task_id):
    str_inputs = Path(f'{task_id}_input.txt').read_text().split(',')
    return [int(i) for i in str_inputs]


def compute(inputs, noun=12, verb=2):
    inputs[1] = noun
    inputs[2] = verb

    i = 0
    while True:
        opcode = inputs[i]
        if opcode == 99:
            return inputs[0]
        op1, op2 = inputs[inputs[i + 1]], inputs[inputs[i + 2]]
        if opcode == 1:
            res = op1 + op2
        elif opcode == 2:
            res = op1 * op2
        else:
            raise ValueError
        inputs[inputs[i + 3]] = res
        i += 4


def find_noun_verb(inputs, output):
    found = False
    for noun in range(100):
        for verb in range(100):
            if compute(inputs.copy(), noun, verb) == output:
                found = True
                break
        if found:
            break
    return noun, verb


if __name__ == '__main__':
    inputs = get_inputs(TASK_ID)

    noun, verb = find_noun_verb(inputs, output=19690720)

    print('1. first value:', compute(inputs))
    print('2. 100 * noun + verb:', 100 * noun + verb)