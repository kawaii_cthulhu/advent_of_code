from c2_1202_program_alarm import get_inputs


TASK_ID = 5


def compute(inputs, comp_inputs, noun=12, verb=2, return_out=True):
    # mode 0 - position (default)
    # mode 1 - immediate

    i = 0
    while True:
        opcode_modes = str(inputs[i])
        opcode = int(opcode_modes[-2:])
        if opcode == 99:
            return inputs[0]

        if len(opcode_modes) < 3 or opcode_modes[-3] == '0':
            idx1 = inputs[i + 1]
        elif opcode_modes[-3] == '1':
            idx1 = i + 1
        if len(opcode_modes) < 4 or opcode_modes[-4] == '0':
            idx2 = inputs[i + 2]
        elif opcode_modes[-4] == '1':
            idx2 = i + 2

        if opcode in (1, 2):
            op1, op2 = inputs[idx1], inputs[idx2]
            if opcode == 1:
                res = op1 + op2
            elif opcode == 2:
                res = op1 * op2
            inputs[inputs[i + 3]] = res
            i += 4
            continue

        if opcode == 3:
            inputs[inputs[i + 1]] = next(comp_inputs)
        elif opcode == 4:
            if return_out:
                return inputs[inputs[i + 1]]
            else:
                print(inputs[inputs[i + 1]])
        elif opcode == 5:
            if inputs[idx1]:
                i = inputs[idx2]
            else:
                i += 3
            continue
        elif opcode == 6:
            if not inputs[idx1]:
                i = inputs[idx2]
            else:
                i += 3
            continue
        elif opcode == 7:
            if inputs[idx1] < inputs[idx2]:
                inputs[inputs[i + 3]] = 1
            else:
                inputs[inputs[i + 3]] = 0
            i += 4
            continue
        elif opcode == 8:
            if inputs[idx1] == inputs[idx2]:
                inputs[inputs[i + 3]] = 1
            else:
                inputs[inputs[i + 3]] = 0
            i += 4
            continue
        else:
            raise ValueError

        i += 2


if __name__ == '__main__':
    system_id = 1  # air conditioner unit
    system_id2 = 5  # thermal radiator controller

    inputs = get_inputs(TASK_ID)
    print('1. air conditioner unit diagnostics')
    compute(inputs.copy(), comp_input=[system_id])
    print('2. thermal radiator controller diagnostics')
    compute(inputs, comp_input=[system_id2])