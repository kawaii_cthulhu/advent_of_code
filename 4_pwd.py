def part1(n1, n2):
    count = 0
    for i in range(n1, n2):
        s = str(i)
        two_same = False
        non_decreasing = True
        for i, c in enumerate(s[:-1]):
            if not two_same and c == s[i + 1]:
                two_same = True
            if c > s[i + 1]:
                non_decreasing = False
                break
        if two_same and non_decreasing:
            count += 1
    return count


def part2(n1, n2):
    count = 0
    for i in range(n1, n2):
        s = str(i)
        two_same = False
        non_decreasing = True
        for i, c in enumerate(s[:-1]):
            if not two_same:
                if c == s[i + 1] and (i + 2 >= len(s) or s[i + 2] != c) and (i - 1 < 0 or s[i - 1] != c):
                    two_same = True
            if c > s[i + 1]:
                non_decreasing = False
                break
        if two_same and non_decreasing:
            count += 1
    return count


if __name__ == '__main__':
    print('1. non-decreasing, at least two same digits', part1(353096, 843212))
    print('2. non-decreasing, two same digits', part2(353096, 843212))