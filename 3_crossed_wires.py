from pathlib import Path


def get_steps():
    steps1, steps2 = Path('3_input.txt').read_text().split('\n')
    return steps1.split(','), steps2.split(',')


def get_wire_segments(steps):
    pos = (0, 0)
    wire = [pos]
    for step in steps:
        direction = step[0]
        count = int(step[1:])
        if direction == 'R':
            pos = (pos[0] + count, pos[1])
        elif direction == 'L':
            pos = (pos[0] - count, pos[1])
        elif direction == 'U':
            pos = (pos[0], pos[1] + count)
        elif direction == 'D':
            pos = (pos[0], pos[1] - count)
        else:
            raise ValueError
        wire.append(pos)

    return wire


def get_wire_points(steps):
    pos = (0, 0)
    points = {}
    steps_to_point = 0
    for step in steps:
        direction = step[0]
        count = int(step[1:])
        if direction == 'R':
            for i in range(1, count + 1):
                steps_to_point += 1
                points[(pos[0] + i, pos[1])] = steps_to_point
            pos = (pos[0] + count, pos[1])
        elif direction == 'L':
            for i in range(1, count + 1):
                steps_to_point += 1
                points[(pos[0] - i, pos[1])] = steps_to_point
            pos = (pos[0] - count, pos[1])
        elif direction == 'U':
            for i in range(1, count + 1):
                steps_to_point += 1
                points[(pos[0], pos[1] + i)] = steps_to_point
            pos = (pos[0], pos[1] + count)
        elif direction == 'D':
            for i in range(1, count + 1):
                steps_to_point += 1
                points[(pos[0], pos[1] - i)] = steps_to_point
            pos = (pos[0], pos[1] - count)
        else:
            raise ValueError

    return points


def intersect(p11, p12, p21, p22):
    if sum(p11) < sum(p12):
        p11, p12 = p12, p11
    if sum(p21) < sum(p22):
        p21, p22 = p22, p21
    x1, y1 = p11
    x2, y2 = p12
    x3, y3 = p21
    x4, y4 = p22
    denom = ((x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4))
    if not denom:  # segments are parallel
        return None
    t = ((x1 - x3) * (y3 - y4) - (y1 - y3) * (x3 - x4)) / denom
    if 0 <= t <= 1:
        px = x1 + t * (x2 - x1)
        py = y1 + t * (y2 - y1)
        return px, py

    return None


def find_intersections(wire1, wire2):
    intersections = []
    for i, point1 in enumerate(wire1[:-1]):
        for j, point2 in enumerate(wire2[:-1]):
            p = intersect(point1, wire1[i + 1], point2, wire2[j + 1])
            if p:
                intersections.append(p)
    return intersections


def points_solution():
    steps1, steps2 = get_steps()
    wire1 = get_wire_points(steps1)
    wire2 = get_wire_points(steps2)
    intersections = set(wire1.keys()).intersection(set(wire2.keys()))
    min_intersection = min(intersections, key=lambda x: abs(x[0]) + abs(x[1]))  # 1017
    min_steps_intersection = min(intersections, key=lambda x: wire1[x] + wire2[x])  # 11432
    print('1. min intersection dist:', abs(min_intersection[0]) + abs(min_intersection[1]))
    print('2. min intersection steps dist:', wire1[min_steps_intersection] + wire2[min_steps_intersection])


def segments_solution():
    steps1, steps2 = get_steps()
    wire1 = get_wire_segments(steps1)
    wire2 = get_wire_segments(steps2)
    intersections = find_intersections(wire1, wire2)
    min_intersection = min(intersections, key=lambda x: abs(x[0]) + abs(x[1]))  # 55 too low
    print('1. min intersection dist:', abs(min_intersection[0]) + abs(min_intersection[1]))


if __name__ == '__main__':
    points_solution()
