import datetime
import operator
from collections import defaultdict, Counter


def read_events():
    with open('4_input.txt') as f:
        data_lines = f.read().strip().split('\n')

    data = []
    for line in data_lines:
        date_time, event_str = line.split('] ')
        date_time = datetime.datetime.strptime(date_time, '[%Y-%m-%d %H:%M')
        data.append((date_time, event_str))
    data.sort(key=operator.itemgetter(0))
    data = [(event[0].minute, event[1]) for event in data]
    return data


def get_guard_id(event):
    return int(event.split('#')[1].split()[0])


def get_sleep_stats(events):
    guards_sleep = defaultdict(Counter)
    cur_guard = 0
    fallen_asleep = 0
    for minute, event in events:
        if event.startswith('G'):
            cur_guard = get_guard_id(event)
        elif event.startswith('f'):
            fallen_asleep = minute
        else:
            guards_sleep[cur_guard].update(range(fallen_asleep, minute))
    return guards_sleep


def get_idxminute(sleep_stats):
    guard_id, stats = max(sleep_stats.items(),
                          key=lambda x: sum(x[1].values()))
    minute = stats.most_common(1)[0][0]
    return guard_id * minute


def most_sleeped_minute(sleep_stats):
    guard = 0
    minute = 0
    times = 0
    for guard_id, stats in sleep_stats.items():
        cur_minute, count = stats.most_common(1)[0]
        if count > times:
            guard = guard_id
            minute = cur_minute
            times = count
    return guard * minute


if __name__ == '__main__':
    events = read_events()
    guards_sleep = get_sleep_stats(events)
    print('1.', get_idxminute(guards_sleep))
    print('2.', most_sleeped_minute(guards_sleep))

