import string


def get_polymer():
    with open('5_input.txt') as f:
        polymer = f.read().strip()
    return polymer


def run_reaction(polymer):
    l = len(polymer)
    while True:
        for letter in string.ascii_lowercase:
            polymer = polymer.replace(letter + letter.upper(), '')
            polymer = polymer.replace(letter.upper() + letter, '')
        if len(polymer) == l:
            break
        l = len(polymer)
    return polymer


def find_min_possible_len(polymer):
    min_len = len(polymer)
    for letter in string.ascii_lowercase:
        cur_polymer = polymer.replace(letter, '').replace(letter.upper(), '')
        cur_result = run_reaction(cur_polymer)
        if len(cur_result) < min_len:
            min_len = len(cur_result)
    return min_len


if __name__ == '__main__':
    polymer = get_polymer()
    polymer_reacted = run_reaction(polymer)
    print('1.', len(polymer_reacted))
    print('2.', find_min_possible_len(polymer))
