import numpy as np

ROW_SIZE = 300
SQUARE_SIZE = 3


def fill_grid(sid):
    grid = []
    for y in range(1, ROW_SIZE + 1):
        row = []
        for x in range(1, ROW_SIZE + 1):
            rack_id = x + 10
            p_level = (rack_id * y + sid) * rack_id
            if p_level < 100:
                p_level = 0
            else:
                p_level = int(str(p_level)[-3])
            row.append(p_level - 5)
        grid.append(row)
    return grid


def find_max(grid, square_size=SQUARE_SIZE):
    max_x = max_y = 0
    max_p = -100
    for x in range(len(grid) - square_size + 1):
        for y in range(len(grid) - square_size + 1):
            p = np.sum(grid[y:y+square_size,x:x+square_size])
            if p > max_p:
                max_p = p
                max_x = x
                max_y = y
    return max_x + 1, max_y + 1, max_p


def find_max_any_size(grid):
    max_p = -10000
    size = 0
    for i in range(1, len(grid) + 1):
        print(i)
        x, y, p = find_max(grid, i)
        if p > max_p:
            max_p = p
            max_x = x
            max_y = y
            size = i
    return max_x, max_y, size


if __name__ == '__main__':
    sid = 9424
    grid = fill_grid(sid)
    grid = np.array(grid)
    print('1.', find_max(grid)[:-1])
    print('2.', find_max_any_size(grid))
