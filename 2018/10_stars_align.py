#import matplotlib.pyplot as plt


class Point:
    def __init__(self, x, y, v_x, v_y):
        self.x = x
        self.y = y
        self.v_x = v_x
        self.v_y = v_y

    def move(self):
        self.x += self.v_x
        self.y += self.v_y

    def __str__(self):
        return 'Point({0}, {1}, {2}, {3})'.format(self.x, self.y,
                                                  self.v_x, self.v_y)

    def __repr__(self):
        return str(self)


def get_positions():
    with open('10_input.txt') as f:
        lines = f.read().strip().split('\n')

    positions = []
    for line in lines:
        line = line[10:-1]
        p_x, line, v_y = line.split(', ')
        p_y = line.split('>')[0]
        v_x = line.split('<')[1]
        positions.append(Point(int(p_x), int(p_y), int(v_x), int(v_y)))
    return positions


def get_frame(positions):
    min_x = min(positions, key=lambda x: x.x).x
    max_x = max(positions, key=lambda x: x.x).x
    min_y = min(positions, key=lambda x: x.y).y
    max_y = max(positions, key=lambda x: x.y).y
    return min_x, min_y, max_x, max_y


#def show_graph(positions):
#    x = [p.x for p in positions]
#    y = [p.y for p in positions]
#    mng = plt.get_current_fig_manager()
#    mng.resize(*mng.window.maxsize())
#    plt.scatter(x, y, marker='.', s=1)
#    plt.show()


def go(positions, seconds):
    for i in range(seconds):
        for p in positions:
            p.move()
        min_x, min_y, max_x, max_y = get_frame(positions)
        h = max_y - min_y + 1
        w = max_x - min_x + 1

        if h < 30:
            print(i, w, h)
            pos = {(p.x, p.y) for p in positions}
            for y in range(min_y, max_y + 1):
                print(''.join(['#' if (x, y) in pos else '.'
                               for x in range(min_x, max_x + 1)]))
            print()


if __name__ == '__main__':
    positions = get_positions()
    go(positions, 50000)  # PHLGNRFK in lowercase, took 10407 seconds
