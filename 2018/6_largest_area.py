from collections import defaultdict


def get_coordinates():
    with open('6_input.txt') as f:
        txt = f.read().strip().split('\n')
    coordinates = set()
    for line in txt:
        x, y = line.split(', ')
        coordinates.add((int(x), int(y)))
    return coordinates


def get_frame(coordinates):
    min_x = min(coordinates, key=lambda x: x[0])[0]
    max_x = max(coordinates, key=lambda x: x[0])[0]
    min_y = min(coordinates, key=lambda x: x[1])[1]
    max_y = max(coordinates, key=lambda x: x[1])[1]
    return min_x, min_y, max_x, max_y


def manhattan_distance(x1, y1, x2, y2):
    return abs(x2 - x1) + abs(y2 - y1)


def show_coords(coordinates):
    min_x, min_y, max_x, max_y = get_frame(coordinates)
    for y in range(min_y, max_y + 1):
        s = ''
        for x in range(min_x, max_x + 1):
            if (x, y) in coordinates:
                s += 'A'
            else:
                s += '.'
        print(s)


def get_areas(coordinates):
    areas = defaultdict(set)
    min_x, min_y, max_x, max_y = get_frame(coordinates)
    infinite = set()
    for x in range(min_x, max_x + 1):
        for y in range(min_y, max_y + 1):
            min_dist = max_x + max_y
            closest = [(x, y)]
            for c in coordinates:
                dist = manhattan_distance(x, y, *c)
                if dist < min_dist:
                    min_dist = dist
                    closest = [c]
                elif dist == min_dist:
                    closest.append(c)
            if len(closest) == 1:
                c = closest[0]
                areas[c].add((x, y))
                if y in (min_y, max_y) or x in (min_x, max_x):
                    infinite.add(c)
    return areas, infinite


def get_region_size(coordinates):
    min_x, min_y, max_x, max_y = get_frame(coordinates)
    region_size = 0
    for x in range(min_x, max_x + 1):
        for y in range(min_y, max_y + 1):
            dist = sum((manhattan_distance(x, y, *c) for c in coordinates))
            region_size += dist < 10000

    return region_size


def get_largest_area(areas, infinite):
    for a in infinite:
        areas.pop(a)
    largest_area, points = max(areas.items(), key=lambda x: len(x[1]))
    return len(points)


if __name__ == '__main__':
    coordinates = get_coordinates()
    print('1.', get_largest_area(*get_areas(coordinates)))
    print('2.', get_region_size(coordinates))


