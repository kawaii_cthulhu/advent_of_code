def read_frequencies():
    with open('1_input.txt') as f:
        frequencies = f.read().split('\n')
    return [int(f) for f in frequencies if f]


def get_final(frequencies, initial_f):
    result_f = initial_f
    for f in frequencies:
        result_f += f
    return result_f


def get_first_twice(frequencies, initial_f):
    result_f = initial_f
    seen = {initial_f}
    while True:
        for f in frequencies:
            result_f += f
            if result_f in seen:
                print(len(seen))
                return result_f
            seen.add(result_f)



if __name__ == '__main__':
    frequencies = read_frequencies()
    initial_f = 0
    print('1. final: ', get_final(frequencies, initial_f))
    print('2. frequency seen twice first:', get_first_twice(frequencies,
                                                            initial_f))
