from collections import Counter

def get_boxes():
    with open('2_input.txt') as f:
        boxes = f.read().split('\n')
    return [b for b in boxes if b]


def get_2_3(boxes):
    two = 0
    three = 0
    for b in boxes:
        c = Counter(b)
        cc = Counter(c.values())
        two += 2 in cc
        three += 3 in cc

    return two * three


def differs_by_one(s1, s2):
    diff = 0
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            diff += 1
            if diff > 1:
                #print('false')
                return False
    if diff == 1:
        #print('true')
        return True


def get_common(boxes):
    found = False
    for i in range(len(boxes)):
        if found:
            break
        for j in range(len(boxes)):
            if i == j:
                continue
            b1, b2 = boxes[i], boxes[j]
            if differs_by_one(b1, b2):
                found = True
                break
    if not found:
        return
    common = ''.join([b1[i] for i in range(len(b1)) if b1[i] == b2[i]])
    return common


if __name__ == '__main__':
    boxes = get_boxes()
    print('1.', get_2_3(boxes))
    print('2.', get_common(boxes))
