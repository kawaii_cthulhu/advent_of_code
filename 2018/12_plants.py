GENERATIONS = 20


def get_plants():
    with open('12_input.txt') as f:
        initial = f.readline().split()[-1]
        f.readline()
        rules = f.read().strip().split('\n')

    rules = {line.split(' => ')[0]: line.split(' => ')[1] for line in rules}
    return initial, rules


def grow(plants, rules, generations=GENERATIONS, part2=False):
    #print(0, plants)
    pots = ([(i, '.') for i in range(-100 * 2, 0)] +
            [(i, plants[i]) for i in range(len(plants))] +
            [(i, '.') for i in range(len(plants), len(plants) + 100)])
    # theory_proved = True  # part 2
    for gen in range(generations):
        next_gen = []
        for i, p in enumerate(pots):
            if i < 2:
                rule_str = '.' * (2-i) + ''.join([pot[1]
                                                  for pot in pots[i:i+3]])
            elif i + 2 >= len(pots):
                rule_str = (''.join([pot[1] for pot in pots[i-2:i+2]]) +
                            '.' * (i + 3 - len(pots)))
            else:
                rule_str = ''.join([pot[1] for pot in pots[i-2:i+3]])
            next_gen.append((p[0], rules.get(rule_str, '.')))
        pots = next_gen
        #print(gen + 1, ''.join(p[1] for p in pots))
        if not part2:
            continue

        # for second part
        vals = [p[0] for p in pots if p[1] == '#']
        if gen > 90:
            #theory_proved = theory_proved and vals == [v+1 for v in prev_vals]
            break

    if part2:
        val = [v + generations - 1 - gen for v in vals]
    else:
        val = [p[0] for p in pots if p[1] == '#']
    return sum(val)


if __name__ == '__main__':
    initial, rules = get_plants()
    print('1.', grow(initial, rules))  # 1447
    print('2.', grow(initial, rules, 50000000000, True))  # 1050000000480
