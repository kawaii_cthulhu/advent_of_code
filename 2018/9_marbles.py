def read_input():
    with open('9_input.txt') as f:
        line = f.read().strip()
    line = line.split()
    players = int(line[0])
    last_marble = int(line[-2])
    return players, last_marble


class Node:
    def __init__(self, value, prev=None, next=None):
        self.value = value
        self.prev = prev or self
        self.next = next or self


def play(players, last_marble):
    players = [0] * players
    player = 0
    cur_marble = Node(0)
    for marble in range(1, last_marble + 1):
        if not marble % 23:
            m = cur_marble
            for i in range(7):
                m = m.prev

            m.next.prev = m.prev
            cur_marble = m.prev.next = m.next

            players[player] += marble
            players[player] += m.value
        else:
            m1 = cur_marble.next
            m2 = m1.next
            cur_marble = Node(marble, m1, m2)
            m1.next = m2.prev = cur_marble

        player += 1
        if player == len(players):
            player = 0

    return max(players)


def play_list(players, last_marble):
    players = [0] * players
    player = 1
    marbles = [0, 1]
    cur_marble_idx = 1
    for marble in range(2, last_marble + 1):
        if not marble % 23:
            players[player] += marble
            remove_idx = cur_marble_idx - 7
            players[player] += marbles.pop(remove_idx)
            cur_marble_idx = remove_idx if remove_idx < len(marbles) else 0
            if cur_marble_idx < 0:
                cur_marble_idx = len(marbles) - cur_marble_idx
            player += 1
            if player == len(players):
                player = 0
            continue

        idx = cur_marble_idx + 2
        if idx <= len(marbles):
            marbles.insert(idx, marble)
        else:
            idx = idx - len(marbles)
            marbles.insert(idx, marble)
        cur_marble_idx = idx

        player += 1
        if player == len(players):
            player = 0

    return max(players)


if __name__ == '__main__':
    players, last_marble = read_input()
    print('1.', play(players, last_marble))
    print('2.', play(players, last_marble * 100))
