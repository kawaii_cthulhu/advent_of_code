from collections import namedtuple


Car = namedtuple('Car', 'row col dir')
DIRECTIONS = {'<': (0, -1),
              '>': (0, 1),
              'v': (1, 0),
              '^': (-1, 0)}


def get_tracks():
    with open('13_input.txt') as f:
        rows = f.read().split('\n')

    return [list(row) for row in rows]


def drive(tracks):
    """
    Each time a cart has the option to turn,
     it turns left the first time,
     goes straight the second time,
     turns right the third time,
     and then repeats those directions
     starting again with left the fourth time, straight the fifth time, and so on.
    :param tracks:
    :return:
    """
    print(tracks)
    crash = False
    prev_state = tracks
    # start positions on straight tracks only
    # get positions of all cars
    # sort
    # move

    cars = []
    for i, row in enumerate(tracks):
        for j, cell in enumerate(row):
            if cell in (' ', '-', '|', '\\', '/', '+'):
                continue
            cars.append(Car(i, j, cell))


    new_cars = []
    for c in cars:
        #todo: check current cell
        if c.dir == '<':  # TODO: add dir (1,2)
            if tracks[c.row][c.col - 1] == '-' and prev_state[c.row][c.col] == '-':
                tracks[c.row][c.col - 1] = c.dir
                tracks[c.row][c.col] = prev_state[c.row][c.col]  # '-' on the first move
                new_cars.append(Car(c.row, c.col - 1, c.dir))




if __name__ == '__main__':
    tracks = get_tracks()
    print('1.', drive(tracks))
