from collections import namedtuple


Node = namedtuple('Node', 'children_count meta_count children meta parent children_and_own_meta')


def get_numbers():
    with open('8_input.txt') as f:
        tree_str = f.read()
    numbers = [int(num) for num in tree_str.split()]
    return numbers


def build_tree(tree):
    tree_root = Node(tree[0], tree[1], [], [], None, [])
    tree_root.meta.extend(tree[-tree_root.meta_count:])
    tree_root.children_and_own_meta.extend(tree[-tree_root.meta_count:])
    tree = tree[2:-tree_root.meta_count]

    while len(tree_root.children) < tree_root.children_count:
        child_node, tree = add_child(tree, tree_root)
        tree_root.children_and_own_meta.extend(child_node.children_and_own_meta)
    return tree_root


def add_child(tree, root):
    node = Node(tree[0], tree[1], [], [], root, [])
    tree = tree[2:]
    root.children.append(node)
    while len(node.children) < node.children_count:
        child_node, tree = add_child(tree, node)
        node.children_and_own_meta.extend(child_node.children_and_own_meta)
    node.children_and_own_meta.extend(tree[:node.meta_count])
    node.meta.extend(tree[:node.meta_count])
    tree = tree[node.meta_count:]
    return node, tree


def get_node_value(node):
    if not node.children_count:
        return sum(node.meta)
    value = 0
    for entry in node.meta:
        if not entry:
            continue
        try:
            value += get_node_value(node.children[entry - 1])
        except IndexError:
            pass
    return value


if __name__ == '__main__':
    numbers = get_numbers()
    root = build_tree(numbers)
    print('1.', sum(root.children_and_own_meta))  # 41849
    print('2.', get_node_value(root))
