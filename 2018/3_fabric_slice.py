from collections import defaultdict


def get_slices():
    with open('3_input.txt') as f:
        slices_str = f.read().split('\n')
    slices = {}
    for s in slices_str:
        if not s:
            continue
        sid, s = s.split(' @ ')
        sid = sid.strip()[1:]
        xy, wh = s.split(': ')
        x, y = xy.split(',')
        w, h = wh.split('x')
        slices[sid] = [int(x), int(y), int(x) + int(w), int(y) + int(h), sid]
    return slices


def get_sliced_fabric(slices):
    sliced_fabric = defaultdict(int)
    for s in slices.values():
        for i in range(s[0], s[2]):
            for j in range(s[1], s[3]):
                sliced_fabric[(i, j)] += 1
    return sliced_fabric


def get_many_claims(sliced_fabric):
    many_claims = 0
    for k in sliced_fabric:
        if sliced_fabric[k] > 1:
            many_claims += 1
    return many_claims


def find_the_one(sliced_fabric):
    for sid, s in slices.items():
        used_twice = False
        for i in range(s[0], s[2]):
            if used_twice:
                break
            for j in range(s[1], s[3]):
                if sliced_fabric[(i, j)] > 1:
                    used_twice = True
                    break
        if not used_twice:
            return s[-1]

if __name__ == '__main__':
    slices = get_slices()
    sliced_fabric = get_sliced_fabric(slices)
    print('1.', get_many_claims(sliced_fabric))
    print('2.', find_the_one(sliced_fabric))