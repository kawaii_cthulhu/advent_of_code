def get_requirements():
    with open('7_input.txt') as f:
        reqs_str = f.read().strip().split('\n')
    reqs = {(r[5], r[36]) for r in reqs_str}
    return reqs


def get_order(reqs):
    order = ''
    not_ready = {r[0] for r in reqs}.union({r[1] for r in reqs})
    while not_ready:
        dependent = {r[1] for r in reqs}
        ready = not_ready - dependent
        next_step = min(ready)
        order += next_step
        not_ready.remove(next_step)
        reqs = {r for r in reqs if r[0] != next_step}
    return order


def do_work(reqs):
    clock = 0
    not_ready = {r[0] for r in reqs}.union({r[1] for r in reqs})
    workers = 5
    done = set()
    in_progress = {}
    while not_ready or in_progress:
        # check what's done
        done_now = set()
        for step in in_progress:
            if in_progress[step] == clock:
                done_now.add(step)
                workers += 1
        for step in done_now:
            in_progress.pop(step)
        done.update(done_now)

        reqs = {r for r in reqs if r[0] not in done_now}

        # check what's ready
        dependent = {r[1] for r in reqs}
        ready_to_start = not_ready - dependent

        # start next steps
        next_steps = sorted(ready_to_start)[:workers]
        workers -= len(next_steps)
        for step in next_steps:
            in_progress[step] = clock + (ord(step) - 4)

        not_ready.difference_update(set(next_steps))
        #print(clock, '\t', '\t'.join([i for i in in_progress]), '\t',
        #      ''.join(sorted(done)), '\t', ready_to_start, '\t', dependent)

        clock += 1
    return clock - 1


if __name__ == '__main__':
    reqs = get_requirements()
    print(reqs)
    print('1.', get_order(reqs))
    print('2.', do_work(reqs))
