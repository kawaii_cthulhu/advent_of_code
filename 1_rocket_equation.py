import math
from pathlib import Path


def read_modules_weights():
    weights = Path('1_input.txt').read_text().split('\n')
    return [int(f) for f in weights if f]


def get_fuel_amount(module_weight):
    return math.floor(module_weight / 3) - 2


def get_fuel_fuel_amount(module_weight):
    total_fuel_weight = get_fuel_amount(module_weight)
    cur_fuel_fuel_weight = total_fuel_weight
    while True:
        cur_fuel_fuel_weight = get_fuel_amount(cur_fuel_fuel_weight)
        if cur_fuel_fuel_weight > 0:
            total_fuel_weight += cur_fuel_fuel_weight
        else:
            break
    return total_fuel_weight


if __name__ == '__main__':
    weights = read_modules_weights()
    print('1. modules fuel amount: ', sum(get_fuel_amount(w) for w in weights))
    print('2. total fuel amount:', sum(get_fuel_fuel_amount(w) for w in weights))