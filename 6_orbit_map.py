from pathlib import Path


def get_orbits():
    lines = Path('6_input.txt').read_text().split('\n')
    orbits = {}
    for l in lines:
        a, b = l.split(')')
        orbits[b] = a  # b orbits a

    return orbits


def count_orbits(orbits):
    total_orbits = 0
    for orbiting in orbits:
        total_orbits += 1
        while not orbits[orbiting] == 'COM':
            total_orbits += 1
            orbiting = orbits[orbiting]

    return total_orbits


def reach_santa(orbits):
    me = orbits['YOU']
    santa = orbits['SAN']

    my_path = []
    while me != 'COM':
        me = orbits[me]
        my_path.append(me)

    santas_path = []
    while santa not in my_path:
        santa = orbits[santa]
        santas_path.append(santa)

    # santa is now at the intersection
    my_path = my_path[:my_path.index(santa) + 1]
    steps = len(santas_path) + len(my_path)

    return steps


if __name__ == '__main__':
    orbits = get_orbits()
    print('1. total orbits count:', count_orbits(orbits))
    print('2. steps to Santa:', reach_santa(orbits))
