from itertools import permutations

from c5_TEST import compute
from c2_1202_program_alarm import get_inputs


TASK_ID = 7


def find_max_output(inputs):
    phases_options = permutations((0, 1, 2, 3, 4), 5)
    outs = []
    for phases in phases_options:
        in_signal = 0
        for phase in phases:
            in_signal = compute(inputs.copy(), iter([phase, in_signal]),
                                return_out=True)
        outs.append(in_signal)

    return max(outs)


def find_max_output_feedback_loop(inputs):
    phases_options = permutations((5, 6, 7, 8, 9), 5)
    outs = []
    for phases in phases_options:
        in_signal = 0
        while True:
            phases_inputs = zip(phases, [inputs.copy() for i in range(len(phases))])
            for phase, cur_input in phases_inputs:
                in_signal = compute(cur_input, iter([phase, in_signal]),
                                    return_out=True)
            if in_signal == cur_input[0]:
                break
        outs.append(in_signal)

    return max(outs)


if __name__ == '__main__':
    inputs = get_inputs(TASK_ID)

    print('1. max output:', find_max_output(inputs))
    print('2. max output using feedback loop:', find_max_output_feedback_loop(inputs))
